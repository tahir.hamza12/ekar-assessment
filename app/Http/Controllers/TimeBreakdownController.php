<?php


/**
 * @license Apache 2.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeBreakdown;
use DateTime;
use Carbon\Carbon;

/**
 * Class TimeBreakdownController
 *
 * * @author  Hamza Tahir <tahir.hamza12@gmail.com>
 */

class TimeBreakdownController extends ApiController
{
     /**
     * @OA\Get(
     *     path="/api/calculate",
     *     tags={"timebreakdown"},
     *     summary="Generates time breakdown as per the unit list",
     *     operationId="calculate",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\Header(
     *             header="X-Rate-Limit",
     *             description="calls per hour allowed by the user",
     *             @OA\Schema(
     *                 type="integer",
     *                 format="int32"
     *             )
     *         ),
     *         @OA\Header(
     *             header="X-Expires-After",
     *             description="date in UTC when token expires",
     *             @OA\Schema(
     *                 type="string",
     *                 format="datetime"
     *             )
     *         ),
     *         @OA\JsonContent(
     *             type="string"
     *         ),
     *         @OA\MediaType(
     *             mediaType="application/xml",
     *             @OA\Schema(
     *                 type="string"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid info supplied"
     *     )
     * )
     */
    public function calculate(Request $request)
    {
    
        $from  = $request->input('from');
        $to  = $request->input('to');
        $intervalSeconds = strtotime($to) - strtotime($from);
        $unitsObject = explode(",",$request->input('unit_list'));
        $unitsInterval = array();
        foreach($unitsObject as $unit){
            $time_interval = preg_replace('/[^0-9]/', '', $unit);
            $time_unit = preg_replace('/[^a-z]/', '', $unit);
            $unitsInterval[$unit] = ($time_interval) > 0 ? (int) $time_interval : 1;
        }
        $res = secondsToStrtime($intervalSeconds,$unitsInterval);
        
        $TimeBreakdown = new TimeBreakdown;
        $TimeBreakdown->start_date_time = (string) $from;
        $TimeBreakdown->end_date_time  = (string) $to;  
        $TimeBreakdown->units_list     = $unitsObject; 
        $TimeBreakdown->duration_breakdown = $res; 

        $this->logResponse($TimeBreakdown);
        
        return $this->successResponse($res, "The timestamp duration breakdown");

    }


    /**
     * Store's response'.
     *
     * @param  App\Model\TimeBreakdown  $TimeBreakdown
     * @return boolean
     */
    public function logResponse(TimeBreakdown $TimeBreakdown)
    {
        $TimeBreakdown->save();
        return true;
    }
    
    /**
     * Store's response'.
     *
     * @param  App\Model\TimeBreakdown  $TimeBreakdown
     * @return boolean
     */
    public function getIntervalls()
    {
        $timebreakdowns = TimeBreakdown::get();
        return $this->successResponse($timebreakdowns, "The timestamp duration breakdown");
    }
    
    /**
     * Store's response'.
     *
     * @param  App\Model\TimeBreakdown  $TimeBreakdown
     * @return boolean
     */
    // public function getIntervall(Request $request)
    // {
    //     $timebreakdown = $request->get();
    //     return $this->successResponse($timebreakdowns, "The timestamp duration breakdown");
    // }

    
}
