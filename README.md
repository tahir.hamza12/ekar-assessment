## Endpoints Structure

#### Get breakdown of timeinterval difference 

```http
  POST     | api/calculate/timeinterval 
```

| Parameter | Type     | Description/Format                |
| :-------- | :------- | :------------------------- |
| ` from`   | `string` | **Required**   yyyy-MM-dd*HH:mm:ss |
| ` to    ` | `string` | **Required** yyyy-MM-dd*HH:mm:ss|
| ` unit_list   ` | `string` |list of time units with interval count: 
                            | Format: 2m,d,3h,,h ( [integer])   


#### Response 

{

    "status": "Success",
    "message": "The timestamp duration breakdown",
        "data": {
            "2m": 1.97,
            "m": 2,
            "d": 59,
            "2h": 1
        }
}

----------

## Installation


Clone the repository


Switch to the repo folder

    cd ekar-assessment

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate
Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

