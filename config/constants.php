<?php 

return [
    'datetime_units' => [
        'y' => ['year', 31104000],
        'm' => ['month', 2592000],
        'w' => ['week', 604800],
        'd' => ['day', 86400],
        'h' => ['hour', 3600],
        'i' => ['minute', 60],
        's' => ['second', 0]
    ]
];