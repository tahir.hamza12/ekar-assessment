<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeBreakdown extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'start_date_time', 'end_date_time'
    ];  
    
    protected $casts = [
        'units_list' => 'array',
        'duration_breakdown' => 'array'
    ];
}
