<?php
use DateTime as _dateTime;

if (!function_exists("secondsToStrtime")) {
    /**
     * Time difference between two dates
     *  @return Object res, difference in the two DateTime objects
     */
    function secondsToStrtime($inputSeconds, array $unit_list)
    {
        $secondsInAMinute = 60;
        $secondsInAnHour  = 60 * $secondsInAMinute;
        $secondsInADay    = 24 * $secondsInAnHour;
        $secondsInAWeek   = 7 * $secondsInADay;
        $secondsInAMonth  = 30 * $secondsInADay;
        
        $res = array();
        
        foreach ($unit_list as $unit => $interval) {
            switch (true) {
                case strstr($unit, 'm'):
                    $months     = round(($inputSeconds / $secondsInAMonth), $interval);
                    $res[$unit] = ($months);
                    break;
                case strstr($unit, 'w'):
                    $weeks      = round(($inputSeconds / $secondsInAWeek), $interval);
                    $res[$unit] = ($weeks) > 0 ? $weeks : 1;
                    break;
                case strstr($unit, 'd'):
                    $days       = round(($inputSeconds / $secondsInADay), $interval);
                    $res[$unit] = ($days) > 0 ? $days : 1;
                    break;
                case strstr($unit, 'h'):
                    $hourSeconds = $inputSeconds % $secondsInADay;
                    $hours       = round(($hourSeconds / $secondsInAnHour), $interval);
                    $res[$unit]  = ($hours) > 0 ? $hours : 1;
                    break;
                case strstr($unit, 'i'):
                    $minuteSeconds = $hourSeconds % $secondsInAnHour;
                    $minutes       = round(($minuteSeconds / $secondsInAMinute), $interval);
                    $res[$unit]    = ($minutes) > 0 ? $minutes : 1;
                    break;
                case strstr($unit, 's'):
                    $remainingSeconds = $minuteSeconds % $secondsInAMinute;
                    $seconds          = ceil($remainingSeconds);
                    $res[$unit]       = ($seconds) > 0 ? $seconds : 1;
                    break;
            }
        }
        
        return $res;
    }
}