<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_breakdowns', function (Blueprint $table) {
            $table->id();
            $table->timestamp('start_date_time')->nullable()->default(null);
            $table->timestamp('end_date_time')->nullable()->default(null);
            $table->json('units_list')->nullable();
            $table->json('duration_breakdown')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_breakdowns');
    }
}
